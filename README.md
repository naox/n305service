### n305service ###
A Swagger enabled RESTful service for demostrating calls to an API using .NET 8.x (Core); moreover, returning the results to a UI built using Angular 17.x. 

### Technologies Used ###
I used the following technologies for development: .NET 8x, Angular 17.x.
I used VS IDE 2022 and VS Code.

### Solution Formulation ###
What I considered while creating the n305 REST Service:
•	SOLID principles.
•	KISS principle.
•	Preferring Repository Pattern for isolation of data access from business logic.
•	Testability of methods – narrowed to methods with some sort of business logic. 
    Moreover, using Moq to mock the method in the repository class(es) so the unit tests can focus on the validitiy of the business logic in the Business Logic layer. 
•	Using separate projects for the Models, Data Access, Business Logic, and Interfaces.
•	Decided against creating more unit tests for the UI because of time constraints;
    however, simular to the service teir, REST processes can be mocked using (the TypeScript version of Moq) TypeMoq.
using Moq;
using n305.BL;
using n305.Interfaces.BL;
using n305.Interfaces.DAL;
using n305.Models.DAL;
using NUnit.Framework;

namespace n305BLTests
{
    public class RetrieveAnswerTests
    {
        private IAiAnswers mockAiAnswers { get; set; }
        private Mock<IAiRepository> mockAiDataAccess { get; set; }

        [SetUp]
        public void Setup()
        {
            mockAiDataAccess = new Mock<IAiRepository>();
            mockAiAnswers = new AiAnswers(mockAiDataAccess.Object);
        }

        [Test]
        [TestCase("cat", ProviderEnum.ChatGPT)]
        [TestCase("mat", ProviderEnum.Gemini)]
        public async void SearchAlln305ByTermShouldReturnn305Grouped(string term, ProviderEnum provider)
        {
            var fakeQuestion = new AiQuestion()
            {
                Question = "fake",
                Provider = provider.ToString()
            };

            //assemble
            mockAiDataAccess.Setup(m => m.GetAiAnswer(It.IsAny<AiQuestion>())).ReturnsAsync("fake answer");

            //act
            var expect = await mockAiDataAccess.Object.GetAiAnswer(fakeQuestion);

            //assert
            Assert.IsNotNull(expect);
            Assert.GreaterOrEqual(expect.Length, 1);
        }
    }
}
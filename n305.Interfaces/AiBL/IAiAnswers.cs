﻿using n305.Models.DAL;
using System.Threading.Tasks;

namespace n305.Interfaces.BL
{
    public interface IAiAnswers
    { 
        Task<string> AiAnswer(AiQuestion aiQuestion);
    }
}

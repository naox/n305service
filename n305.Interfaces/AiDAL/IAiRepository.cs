﻿using n305.Models.DAL;
using System.Threading.Tasks;

namespace n305.Interfaces.DAL
{
    public interface IAiRepository
    {
        public Task<string> GetAiAnswer(AiQuestion aiQuestion);
    }
}

﻿using n305.Interfaces.DAL;
using n305.Models.DAL;
using Azure.AI.OpenAI;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace n305.DAL
{
    public class AiRepository : IAiRepository
    {
        readonly IConfiguration _config;
        readonly OpenAIClient _openAIClient;
        readonly string deploymentId;
        readonly string geminiBaseUrl;
        const string applicationJson = "application/json";
        public AiRepository(IConfiguration config)
        {
            _config = config;
            _openAIClient = new OpenAIClient(_config.GetSection("AOAI_KEY").Value);
            deploymentId = _config.GetSection("AOAI_DEPLOYMENTID").Value;
            var myKey = _config.GetSection("GEMINI_KEY").Value;
            geminiBaseUrl = _config.GetSection("GEMINI_BASE_URL").Value?.Replace("myKey",myKey);
        }

        public async Task<string> GetAiAnswer(AiQuestion aiQuestion)
        {
            try
            {
                var chatgpt = ProviderEnum.ChatGPT.ToString().ToLower();
                var gemini = ProviderEnum.Gemini.ToString().ToLower();
                var provider = aiQuestion.Provider.ToLower();

                if(provider == gemini)
                {
                    return await GeminiText(aiQuestion);
                }
                return await ChatGptText(aiQuestion);
            }
            catch
            {
                throw;
            }
        }

        private async Task<string> GeminiText(AiQuestion aiQuestion)
        {
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Clear();
            httpClient.BaseAddress = new Uri(geminiBaseUrl);
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(applicationJson));
           
            var part = new AiRequest() { text = aiQuestion.Question };
            var req = new AiRequests() { parts = new List<AiRequest>() { part } };
            var requestContent = new { contents = new { parts = req.parts } };
            var json = JsonConvert.SerializeObject(requestContent);

            var request = new HttpRequestMessage(HttpMethod.Post, geminiBaseUrl);
            request.Content = new StringContent(json, Encoding.UTF8, applicationJson);

            var response = await httpClient.SendAsync(request);
            var content = await response.Content.ReadAsStringAsync().ConfigureAwait(false);

            if(content != null)
            {
                var obj = JObject.Parse(content);
                var result = obj["candidates"][0]["content"]["parts"][0]["text"]?.ToString();
                return result;
            }
            else
            {
                throw new HttpRequestException();
            }
        }

        private async Task<string> ChatGptText(AiQuestion aiQuestion)
        {
            var chatCompletionOptions = new ChatCompletionsOptions()
            {
                DeploymentName = deploymentId,
                Messages = {
                    new ChatRequestUserMessage(aiQuestion.Question)
                }
            };
            var response = await _openAIClient.GetChatCompletionsAsync(chatCompletionOptions);

            if (response != null)
            {
                var result = response.Value.Choices[0].Message.Content;
                return result;
            }
            else
            {
                throw new HttpRequestException();
            }
        }
    }
}

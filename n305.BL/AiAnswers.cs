﻿using n305.Interfaces.BL;
using n305.Interfaces.DAL;
using n305.Models.DAL;
using System.Threading.Tasks;

namespace n305.BL
{
    public class AiAnswers : IAiAnswers
    {
        private readonly IAiRepository repo;

        public AiAnswers(IAiRepository _repo)
        {
            repo = _repo;
        }

        public async Task<string> AiAnswer(AiQuestion aiQuestion)
        {
            try
            {
                return await repo.GetAiAnswer(aiQuestion);
            }
            catch
            {
                throw;
            }
        }
    }
}

﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace n305.Models.UI
{
    public class AisDisplay
    {
        [JsonPropertyName("aiResults")]
        public List<AiDisplay> Answer { get; set; }
    }
}

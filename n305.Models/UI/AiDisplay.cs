﻿using n305.Models.DAL;
using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace n305.Models.UI
{
    public class AiDisplay
    {
        [JsonPropertyName("answer")]
        public List<AiQuestion> Answer { get; set; }
    }
}

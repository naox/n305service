﻿using Microsoft.AspNetCore.Mvc;

namespace n305.Models.DAL
{
    public class AiQuestion
    {
        [FromQuery(Name = "provider")]
        public string Provider { get; set; }
        [FromQuery(Name = "question")]
        public string Question { get; set; }
    }
}

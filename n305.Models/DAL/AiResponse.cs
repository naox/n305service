﻿using System;
using System.Text.Json.Serialization;

namespace n305.Models.DAL
{
    public class AiResponse: AiQuestion
    {
        [JsonPropertyName("status")]
        public int Status { get; set; }
    }
}

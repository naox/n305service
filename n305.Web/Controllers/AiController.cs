﻿using DotnetGeminiSDK.Client.Interfaces;
using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.RateLimiting;
using Microsoft.Extensions.Logging;
using n305.Interfaces.BL;
using n305.Models.DAL;
using n305.Models.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace n305.Controllers
{
    [ApiController]
    [Route("ai")]
    public class AiController : ControllerBase
    {
        private readonly ILogger<AiController> _logger;
        private readonly IAiAnswers _aiAnswers;
        private readonly IValidator<AiQuestion> _validator;
        readonly IGeminiClient _geminiClient;

        public AiController(IValidator<AiQuestion> validator, ILogger<AiController> logger, IAiAnswers aiAnswers, IGeminiClient geminiClient)
        {
            _logger = logger;
            _aiAnswers = aiAnswers;
            _validator = validator;
            _geminiClient = geminiClient;
        }

        [HttpGet]
        [Route("answer")]
        [EnableRateLimiting("sliding")]
        public async Task<ActionResult<AisDisplay>> GetAnswerFromAi([FromQuery] AiQuestion aiQuestion)
        {
            var isValidResult = await _validator.ValidateAsync(aiQuestion);
            if (!isValidResult.IsValid)
            {
                return ValidationProblem(isValidResult.Errors?.FirstOrDefault().ErrorMessage);
            }
            try
            {

                var result = await _aiAnswers.AiAnswer(aiQuestion);
                return result == null ? NotFound() : Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.InnerException.Message, ex);
                return Problem();
            }
        }

        [HttpGet]
        [Route("providers")]
        public ActionResult<AisDisplay> GetProviders()
        {
            var providers = new List<Provider>();

            foreach (var value in Enum.GetValues(typeof(ProviderEnum)))
            {
                providers.Add(new Provider() { value = value.ToString().ToLower(), viewValue = value.ToString()});
            }
            return Ok(providers);
        }
    }
}

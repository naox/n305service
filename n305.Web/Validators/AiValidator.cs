﻿using n305.Models.DAL;
using FluentValidation;

namespace n305.Web.Validators
{
    public class AiValidator: AbstractValidator<AiQuestion>
    {
        public AiValidator() 
        {
            RuleFor(aiQuestion => aiQuestion.Question).NotEmpty();
            RuleFor(aiQuestion => aiQuestion.Provider).NotEmpty();
        }
    }
}

using DotnetGeminiSDK;
using FluentValidation;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.RateLimiting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using n305.BL;
using n305.DAL;
using n305.Interfaces.BL;
using n305.Interfaces.DAL;
using System;
using System.IO;
using System.Reflection;
using System.Threading.RateLimiting;

namespace n305
{
    public class Startup
    {
        public static IConfiguration Configuration { get; private set; }
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

            Configuration = builder.Build();  

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "n305", Version = "v1" });
            });

            services.AddCors(options => options.AddDefaultPolicy(builder =>
            {
                builder.WithOrigins(Configuration.GetSection("AllowedHosts").Value).AllowAnyHeader().AllowAnyMethod();
  
            }));

            services.AddRateLimiter(_ => _.AddFixedWindowLimiter(policyName: "sliding", options => {
                options.PermitLimit = 4;
                options.Window = TimeSpan.FromSeconds(4);
                options.QueueProcessingOrder = QueueProcessingOrder.OldestFirst;
                options.QueueLimit = 2;
            }));

            services.AddControllers();
            services.AddTransient<IAiRepository, AiRepository>();
            services.AddTransient<IAiAnswers, AiAnswers>();
            services.AddValidatorsFromAssembly(Assembly.GetExecutingAssembly());

            services.AddGeminiClient(config =>
            {
                config.ApiKey = Configuration.GetSection("GEMINI_KEY").Value;
                config.ImageBaseUrl = Configuration.GetSection("GEMINI_BASE_URL").Value; ;
                config.TextBaseUrl = Configuration.GetSection("GEMINI_BASE_URL").Value; ;
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseDeveloperExceptionPage();
            app.UseSwagger();
            app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "AiService v1"));

            app.UseHttpsRedirection();
           
            app.UseCors(options => options.WithOrigins(Configuration.GetSection("AllowedOrigins").Value).AllowAnyMethod());
            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });     
        }
    }
}
